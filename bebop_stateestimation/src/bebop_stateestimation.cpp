/********************************************************************************
Kumar Bipin
Robotics Research Lab@

International Institute of Information Technology, Hyderabad

This code is developed to test the control command to Bebop Parrot Drone
consequently which are use for System Identification Purpose
*********************************************************************************/
#include "bebop_stateestimation.h"

struct Position bebop_estimate_position(float vx, float vy, float vz, double tk)
{
    struct Position Xt;
    static bool firstRun=false;
    static double  dt=0.02;
    static double dA[] = {
        1.0,  dt,   0.0,  0.0,  0.0,  0.0,
        0.0,  1.0,  0.0,  0.0,  0.0,  0.0,
        0.0,  0.0,  1.0,  dt,   0.0,  0.0,
        0.0,  0.0,  0.0,  1.0,  0.0,  0.0,
        0.0,  0.0,  0.0,  0.0,  1.0,  dt,
        0.0,  0.0,  0.0,  0.0,  0.0,  1.0
    };

    static double dH[] ={
        0.0, 1.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 1.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 1.0
    };
    static double dQ[]={
        1.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 3.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 1.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 3.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 1.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 3.0
    };
    static double dR[]={
        10.0, 0.0,  0.0,
        0.0,  10.0, 0.0,
        0.0,  0.0,  10.0
    };
    /* X=[x,vx,y,vy,z,vz].T */
    static double dX[]={
        0, 0, 0, 0, 0, 0
    };

    static cv::Mat A(6, 6, CV_64FC1);
    static cv::Mat H(3, 6, CV_64FC1);
    static cv::Mat Q(6, 6, CV_64FC1);
    static cv::Mat R(3, 3, CV_64FC1);
    static cv::Mat P(6, 6, CV_64FC1);
    static cv::Mat X(6, 1, CV_64FC1);
    static cv::Mat Z(3, 1, CV_64FC1);

    static cv::Mat Xp(6, 1, CV_64FC1);
    static cv::Mat Pp(6, 6, CV_64FC1);
    static cv::Mat K(6, 1, CV_64FC1);

    static cv::Mat AT(6, 6, CV_64FC1);
    static cv::Mat HT(6, 3, CV_64FC1);
    if(!firstRun)
    {
        firstRun=true;
        A = cv::Mat(6, 6, CV_64FC1, dA);
        cv::transpose(A, AT);
        H = cv::Mat(3, 6, CV_64FC1, dH);
        cv::transpose(H, HT);
        Q = cv::Mat(6, 6, CV_64FC1, dQ);
        R = cv::Mat(3, 3, CV_64FC1, dR);
        P =5*(cv::Mat::eye(6, 6, CV_64F));
        X = cv::Mat(6, 1, CV_64FC1, dX);
        /* Z=[vx,vy,vz] */
        Z = cv::Mat::zeros(3, 1, CV_64F);
        std::cout << "firstRun position estimate ..." << std::endl;
    }

    Z.at<double>(0,0)=vx;
    Z.at<double>(1,0)=vy;
    Z.at<double>(2,0)=vz;

    Xp=A*X;
    Pp=A*P*AT + Q;
    K=Pp*HT*(H*Pp*HT +R).inv();
    X=Xp+K*(Z-H*Xp);
    P=Pp-K*H*Pp;

    Xt.x=X.at<double>(0,0);
    Xt.y=X.at<double>(2,0);
    Xt.z=X.at<double>(4,0);;
    
    return(Xt);
}

struct Acceleration bebop_estimate_acceleration(float vx, float vy, float vz, double tk)
{
    struct Acceleration At;
    static bool firstRun=false;
    static double  dt=0.02;
    static double dA[] = {
        1.0,  dt,   0.0,  0.0,  0.0,  0.0,
        0.0,  1.0,  0.0,  0.0,  0.0,  0.0,
        0.0,  0.0,  1.0,  dt,   0.0,  0.0,
        0.0,  0.0,  0.0,  1.0,  0.0,  0.0,
        0.0,  0.0,  0.0,  0.0,  1.0,  dt,
        0.0,  0.0,  0.0,  0.0,  0.0,  1.0
    };

    static double dH[] ={
        1.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 1.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 1.0, 0.0
    };
    static double dQ[]={
        1.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 3.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 1.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 3.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 1.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 3.0
    };
    static double dR[]={
        10.0, 0.0,  0.0,
        0.0,  10.0, 0.0,
        0.0,  0.0,  10.0
    };
    /* X=[vx, ax, vy, ay, vz.az] */
    static double dX[]={
        0, 0, 0, 0 , 0, 0
    };

    static cv::Mat A(6, 6, CV_64FC1);
    static cv::Mat H(3, 6, CV_64FC1);
    static cv::Mat Q(6, 6, CV_64FC1);
    static cv::Mat R(3, 3, CV_64FC1);
    static cv::Mat P(6, 6, CV_64FC1);
    static cv::Mat X(6, 1, CV_64FC1);
    static cv::Mat Z(3, 1, CV_64FC1);

    static cv::Mat Xp(6, 1, CV_64FC1);
    static cv::Mat Pp(6, 6, CV_64FC1);
    static cv::Mat K(6, 1, CV_64FC1);

    static cv::Mat AT(6, 6, CV_64FC1);
    static cv::Mat HT(6, 3, CV_64FC1);

    if(!firstRun)
    {
        firstRun=true;
        A = cv::Mat(6, 6, CV_64FC1, dA);
        cv::transpose(A, AT);
        H = cv::Mat(3, 6, CV_64FC1, dH);
        cv::transpose(H, HT);
        Q = cv::Mat(6, 6, CV_64FC1, dQ);
        R = cv::Mat(3, 3, CV_64FC1, dR);
        P =5*(cv::Mat::eye(6, 6, CV_64F));
        X = cv::Mat(6, 1, CV_64FC1, dX);
        /* V=[vx,vy,vz] */
        Z = cv::Mat::zeros(3, 1, CV_64F);
        std::cout << "firstRun acceleration estimate..." << std::endl;
    }

    Z.at<double>(0,0)=vx;
    Z.at<double>(1,0)=vy;
    Z.at<double>(2,0)=vz;

    Xp=A*X;
    Pp=A*P*AT + Q;
    K=Pp*HT*(H*Pp*HT +R).inv();
    X=Xp+K*(Z-H*Xp);
    P=Pp-K*H*Pp;

    At.ax=X.at<double>(1,0);;
    At.ay=X.at<double>(3,0);;
    At.az=X.at<double>(5,0);;

    return(At);
}

struct Position bebop_position_kalman_fusion(struct Position Ximu, struct Position Xgps)
{
    struct Position Pt;
    static bool firstRun=false;
    static double dA[] = {
        1.0,  0.0, 0.0,
        0.0,  1.0, 0.0,
        0.0,  0.0, 1.0
    };

    static double dH[] ={
        1.0, 0.0, 0.0,
        0.0, 1.0, 0.0,
        0.0, 0.0, 1.0
    };
    static double dQ[]={
        10.0, 0.0,  0.0,
        0.0,  10.0, 0.0,
        0.0,  0.0,  10.0
    };
    static double dR[]={
        40.0, 0.0,  0.0,
        0.0,  40.0, 0.0,
        0.0,  0.0,  40.0
    };

    /* X=[Xax, Xay, Xaz::Zax, Zay, Zaz].T */
    static double dX[]={
        0, 0, 0
    };

    static cv::Mat A(3, 3, CV_64FC1);
    static cv::Mat H(3, 3, CV_64FC1);
    static cv::Mat Q(3, 3, CV_64FC1);
    static cv::Mat R(3, 3, CV_64FC1);
    static cv::Mat P(3, 3, CV_64FC1);
    static cv::Mat X(3, 1, CV_64FC1);
    static cv::Mat Z(3, 1, CV_64FC1);

    static cv::Mat Xp(3, 1, CV_64FC1);
    static cv::Mat Pp(3, 3, CV_64FC1);
    static cv::Mat K(3, 1, CV_64FC1);

    static cv::Mat AT(3, 3, CV_64FC1);
    static cv::Mat HT(3, 3, CV_64FC1);

    if(!firstRun)
    {
        firstRun=true;
        A = cv::Mat(3, 3, CV_64FC1, dA);
        cv::transpose(A, AT);
        H = cv::Mat(3, 3, CV_64FC1, dH);
        cv::transpose(H, HT);
        Q = cv::Mat(3, 3, CV_64FC1, dQ);
        R = cv::Mat(3, 3, CV_64FC1, dR);
        P =5*(cv::Mat::eye(3, 3, CV_64F));
        /* X=[ax,ay.az] */
        X = cv::Mat(3, 1, CV_64FC1, dX);
        /* Z=[ax,ay,az] */
        Z = cv::Mat::zeros(3, 1, CV_64F);
        //std::cout << "firstRun fusion acceleration" << std::endl;
    }

    X.at<double>(0,0)=Ximu.x;
    X.at<double>(1,0)=Ximu.y;
    X.at<double>(2,0)=Ximu.z;

    Z.at<double>(0,0)=Xgps.x;
    Z.at<double>(1,0)=Xgps.y;
    Z.at<double>(2,0)=Xgps.z;


    Xp=A*X;
    Pp=A*P*AT + Q;
    K=Pp*HT*(H*Pp*HT +R).inv();
    X=Xp+K*(Z-H*Xp);
    P=Pp-K*H*Pp;

    Pt.x=X.at<double>(0,0);
    Pt.y=X.at<double>(1,0);
    Pt.z=X.at<double>(2,0);
    
    return(Pt);
}
