//
//  predict.hpp
//  Feature
//
//  Created by vishakh duggal on 01/11/15.
//  Copyright © 2015 vishakh duggal. All rights reserved.
//

#ifndef predict_hpp
#define predict_hpp
#include "linear.h"
#include <stdio.h>
#include <stdlib.h>
#include <iostream>

#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
cv::Mat predict_lables(cv::Mat features);
#endif /* predict_hpp */
