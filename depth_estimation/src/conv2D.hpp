//
//  conv2D.hpp
//  Feature
//
//  Created by vishakh duggal on 31/10/15.
//  Copyright © 2015 vishakh duggal. All rights reserved.
//

#ifndef conv2D_hpp
#define conv2D_hpp
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include <stdio.h>
enum ConvolutionType {
    /* Return the full convolution, including border */
    CONVOLUTION_FULL,
    
    /* Return only the part that corresponds to the original image */
    CONVOLUTION_SAME,
    
    /* Return only the submatrix containing elements that were not influenced by the border
     */
    CONVOLUTION_VALID
};
void conv2(const cv::Mat &img, const cv::Mat& kernel, ConvolutionType type, cv::Mat& dest);
#endif /* conv2D_hpp */
