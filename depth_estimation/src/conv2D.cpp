//
//  conv2D.cpp
//  Feature
//
//  Created by vishakh duggal on 31/10/15.
//  Copyright © 2015 vishakh duggal. All rights reserved.
//

#include "conv2D.hpp"
#include "iostream"

cv::Mat source;
void conv2(const cv::Mat &img, const cv::Mat& kernel, ConvolutionType type, cv::Mat& dest) {
    
    img.convertTo(source, CV_32FC1);
    cv::Mat result;
    
    cv::Point anchor(kernel.cols - kernel.cols/2 - 1, kernel.rows - kernel.rows/2 - 1);
    int borderMode = cv::BORDER_CONSTANT;
    cv::flip(kernel,result, -1);
    cv::filter2D(source, dest, source.depth(),result, anchor, 0, borderMode);
    
    if(CONVOLUTION_VALID == type) {
        dest = dest.colRange((kernel.cols-1)/2, dest.cols - kernel.cols/2)
        .rowRange((kernel.rows-1)/2, dest.rows - kernel.rows/2);
    }
}