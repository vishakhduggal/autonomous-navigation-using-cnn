#include "staircase_script2_func.hpp"
#include <math.h> 
#include <stdlib.h>

extern "C" {
#include "lsd.h"
}
using namespace std;
using namespace cv;

Point2f lineintersects(Vec4i line1,Vec4i line2)
{
Point2f pt;
double K_Nr_1,K_Nr_2,K_Dr,K;
int x1,y1,x2,y2,x3,y3,x4,y4;

x1=line1[0];
y1=line1[1];
x2=line1[2];
y2=line1[3];
x3=line2[0];
y3=line2[1];
x4=line2[2];
y4=line2[3];
double midx=(x1+x2)/2;
double midy=(y1+y2)/2;
K_Nr_1 = (y4-y3)*(midx-x3);
K_Nr_2 = (x4-x3)*(midy-y3);
K_Dr = pow((y4-y3),2)+pow((x4-x3),2);
K = (K_Nr_1 - K_Nr_2)/K_Dr;
pt.x = midx - (K*(y4-y3)) ;
pt.y = midy + (K*(x4-x3)) ;
//cout << pt << endl;
return pt;
}
Point3f staircase_script2_func(Mat Img,int frame)
{

Mat I;
cvtColor(Img,I,CV_BGR2GRAY);
int row=I.rows;
int col=I.cols;
double *image;
Point3f pt;
vector<Vec4i> lines;
vector<Vec4i> lines_hor;
vector<Vec4i> lines_hor_sel;
image = (double *) malloc( col * row * sizeof(double) );
for (int i=0;i<col;i++){
    for(int j=0;j<row;j++){
        image[ i + j * col ] = I.at<uchar>(j,i);
    }
    }
double *out;
    int n,cnt=0;
    out = lsd_scale( &n, image, col, row ,0.1);
if(n==0)
{
pt.x=0;pt.y=0;pt.z=0;
return pt;
} 
for (int j=0; j<n ; j++){       
        Point2d pt1 = Point(out[ 0 + j * 7 ],out[ 1 + j * 7 ]);
        Point2d pt2 = Point(out[ 2 + j * 7 ],out[ 3 + j * 7 ]);
       float num = pt2.y-pt1.y;
       float den= pt2.x-pt1.x;
    double theta= (atan2(num,den)*180/CV_PI);
    if((abs(theta) >=0 && abs(theta)<=5) || (abs(theta)>=175 && abs(theta)<=180 ))
{
Vec4i l;
l[0]=pt1.x;
l[1]=pt1.y;
l[2]=pt2.x;
l[3]=pt2.y;
     lines.push_back(l);
     cnt=cnt+1;
}
}
//cout << "cnt " << cnt << endl;
double *lines_len;
lines_len = new double[n]();
double max_len=0;
for(int j=0;j<cnt;j++)
{
Vec4i l=lines[j];
lines_len[j]=sqrt(pow((l[2]-l[0]),2)+pow((l[3]-l[1]),2));
if(lines_len[j]>max_len)
max_len=lines_len[j];
}
int cnt_hor=0;
double temp_len=0.2*max_len;
for(int j=0;j<cnt;j++)
{
if(lines_len[j]> temp_len)
{
Vec4i l=lines[j];
lines_hor.push_back(l);
cnt_hor++;
}
}
int* hist_acc;
hist_acc = new int[cnt_hor]();
for(int j=0;j<cnt_hor;j++)
{
Vec4i l=lines_hor[j];
line( Img, Point(l[0], l[1]), Point(l[2], l[3]), Scalar(0,0,255), 1, CV_AA);
}
for(int i=0;i<cnt_hor;i++)
for(int k=0;k<cnt_hor;k++)
{
if(i!=k)
{
Point2f pt = lineintersects(lines_hor[i],lines_hor[k]);
Vec4i l=lines_hor[k];
if(round(pt.x) > min(l[0],l[2]) && round(pt.x) < max(l[0],l[2]) )
{
//line( Img, Point(l[0], l[1]), Point(l[2], l[3]), Scalar(255,255,0), 1, CV_AA);
int x=round(pt.x);
int y=round(pt.y);
hist_acc[i]+=1;
}
}
}
double cnt_sel=0;
for(int k=0;k<cnt_hor;k++)
{
if(hist_acc[k]>0.30*cnt_hor)
{
Vec4i l=lines_hor[k];
lines_hor_sel.push_back(lines_hor[k]);
cnt_sel++;
}
}
int sum1=0,sum2=0;
double conf=0;
if(cnt_sel>5 && cnt_hor!=0)
conf=(cnt_sel/cnt_hor);
for(int k=0;k<cnt_sel;k++)
{
  Vec4i l=lines_hor_sel[k];
  sum1=sum1+l[0]+l[2];
  sum2=sum2+l[1]+l[3];
}

//cout << cnt_hor << endl;
//cout << cnt_sel << endl;

if(cnt_sel>5)
{
for(int k=0;k<cnt_sel;k++)
{
Vec4i l=lines_hor_sel[k];
line( Img, Point(l[0], l[1]), Point(l[2], l[3]), Scalar(0,255,0), 1, CV_AA);
}
}
if(cnt_sel>5)
{
pt.x=sum1/(2*cnt_sel);
pt.y=sum2/(2*cnt_sel);
pt.z=conf;
}
else
{
pt.x=0;pt.y=0;pt.z=conf;
}
delete[] lines_len;
lines_len=0;
Img.release();
I.release();
free(image);
image=0;
return pt;
}
