#ifndef _MID_FUNC
#define _MID_FUNC

#include<iostream>
#include <opencv2/nonfree/features2d.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include<vector>
#include "opencv2/highgui/highgui_c.h"
#include "opencv2/imgproc/imgproc_c.h"
extern "C" {
#include "lsd.h"
}
using namespace std;
using namespace cv;

Point2f lineintersect(Vec4i line1,Vec4i line2);
Point3f staircase_script2_func(Mat img,int frame);

#endif
