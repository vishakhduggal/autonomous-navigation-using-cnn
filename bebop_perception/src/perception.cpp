/*******************************************************************************
Name     :   Kumar Bipin
Email Id :   kumarb@uuurmi.com

Uuurmi System Lab, Hyderabad

This code is developed to test the control command to Ardrone 2.0
consequently which are use for System Identification Purpose
*********************************************************************************/

#include "ros/ros.h"
#include "std_msgs/String.h"
#include <image_transport/image_transport.h>
#include <camera_info_manager/camera_info_manager.h>
#include <sensor_msgs/Image.h>
#include "bebop_perception/NavdataWaypoint.h"

#include <opencv2/highgui/highgui.hpp>
#include <opencv2/nonfree/features2d.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <cv_bridge/cv_bridge.h>

#include <sstream>
#include<vector>

#include "classifierann.hpp"
#include "staircase_script2_func.hpp"
#include "vanishpoint_script2_func.hpp"
#include "Kalman.h"

using namespace cv;
using namespace std;

#define PI 3.14159

std::string encoding;
image_transport::CameraPublisher pub;
image_transport::CameraSubscriber sub_camera;
camera_info_manager::CameraInfoManager *cinfo;
ros::Publisher NavdataWaypoint_pub;

cv::Mat frame;
double *image;
Point2f waypoint, predict;
static cv::Mat frame_prev;
float theta, deltaheading;

bebop_perception::NavdataWaypoint nav_msg;

sensor_msgs::ImagePtr image_msg_ptr;
sensor_msgs::CameraInfoPtr cinfo_msg(new sensor_msgs::CameraInfo(cinfo->getCameraInfo()));

void callback(const sensor_msgs::ImageConstPtr& image_msg, const sensor_msgs::CameraInfoConstPtr& info)
{

    static unsigned int count=0, vp_miss=0, mp_miss=0;
    try
    {
        frame = cv_bridge::toCvShare(image_msg, encoding)->image;
    }
    catch(cv_bridge::Exception)
    {
        ROS_ERROR("Unable to convert %s image to %s", image_msg->encoding.c_str(), encoding.c_str());
        return;
    }

    
    if (!frame.empty()) {

        //putText(frame,"CORRIDOR",cvPoint(30,30),FONT_HERSHEY_COMPLEX, 1.0 ,cvScalar(255,255,0),1,CV_AA);
        putText(frame, "Autonomous Cruise Control", cvPoint(frame.rows-90,frame.cols/2 + 25),FONT_ITALIC,0.6,cvScalar(0,255,0),1.5,CV_AA);
        Point3f vp=vanishpoint_script2_func(frame, count, image); // Calculate vanishing point,pt.z gives confidence
        Point2f pt(vp.x, vp.y);

        std::ostringstream str;
        str << "Algorithum :" << vp.z;            // Confidence of corridor algorithm
        putText(frame, str.str(), cvPoint(30,100),FONT_HERSHEY_COMPLEX,0.8,cvScalar(0,0,250),1,CV_AA);

        waypoint.x=vp.x;
        waypoint.y=vp.y;
        
        circle(frame,Point(waypoint.x,waypoint.y),7,Scalar(0,255,0),-1); // Show the waypoint for Navigation


        image_msg_ptr = cv_bridge::CvImage(std_msgs::Header(), "bgr8", frame).toImageMsg();

        cinfo_msg->header.stamp = image_msg_ptr->header.stamp;
       // cinfo_msg->header.frame_id = image_msg_ptr->header.frame_id;

        cinfo_msg->width = image_msg_ptr->width;
        cinfo_msg->height = image_msg_ptr->height;


        pub.publish(image_msg_ptr, cinfo_msg);
        count++;
        nav_msg.deltaX= (waypoint.x - image_msg_ptr->height/2);
        nav_msg.deltaY= (waypoint.y - image_msg_ptr->width/2);

        theta = atan(nav_msg.deltaX*(1.6/image_msg_ptr->width));
        nav_msg.deltaheading = theta * (180/PI);
        //nav_msg.deltaheading = theta;

        NavdataWaypoint_pub.publish(nav_msg);

    } else {
        ROS_WARN("Frame skipped, no data!");
        return;
    }
}


int main(int argc, char **argv)
{
    ros::init(argc, argv, "ardrone_perception");
    ros::Time::init();
    ros::Rate loop_rate(50);

    ros::NodeHandle nh;

    image = (double *) malloc( 640 * 368 * sizeof(double));
    /* Publishing the Video */
    image_transport::ImageTransport it(nh);
    pub = it.advertiseCamera("bebop_perception/image", 1);

    /* Receiving the Video */
    std::string topic = nh.resolveName("/bebop/front/image_raw");
    sub_camera = it.subscribeCamera(topic, 1, &callback);
    image_transport::Subscriber sub_image = it.subscribe(topic, 1,
                                                         boost::bind(callback, _1, sensor_msgs::CameraInfoConstPtr()));

    cinfo = new camera_info_manager::CameraInfoManager(ros::NodeHandle("bebop_front"), "bebop_front");

    NavdataWaypoint_pub = nh.advertise<bebop_perception::NavdataWaypoint>("/bebop_perception/navdata", 1);

    ros::spin();
    free(image);
    return 0;
}

