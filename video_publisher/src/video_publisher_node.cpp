/****************************************************************************
* Software License Agreement (Apache License)
*
*     Copyright (C) 2012-2013 Open Source Robotics Foundation
*
*     Licensed under the Apache License, Version 2.0 (the "License");
*     you may not use this file except in compliance with the License.
*     You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
*     Unless required by applicable law or agreed to in writing, software
*     distributed under the License is distributed on an "AS IS" BASIS,
*     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*     See the License for the specific language governing permissions and
*     limitations under the License.
*
*****************************************************************************/

#include <opencv2/highgui/highgui.hpp>
#include <iostream>
#include <fstream>
#include <time.h>
#include <fstream>

#include <ros/ros.h>
#include <opencv2/highgui/highgui.hpp>
#include <cv_bridge/cv_bridge.h>
#include <image_transport/image_transport.h>
#include <camera_calibration_parsers/parse.h>
#include <camera_info_manager/camera_info_manager.h>

using namespace cv;
using namespace std;

image_transport::CameraPublisher camera_pub;
camera_info_manager::CameraInfoManager *cinfo;


int main(int argc, char** argv)
{
    ros::init(argc, argv, "video_publisher", ros::init_options::AnonymousName);
    ros::NodeHandle nh;
    ros::Rate loop_rate(30);

    image_transport::ImageTransport it(nh);
    camera_pub = it.advertiseCamera("/bebop/front/image_raw", 10);
    cinfo = new camera_info_manager::CameraInfoManager(ros::NodeHandle("/bebop/front"), "bebop_front");

    sensor_msgs::ImagePtr image_msg_ptr;
    sensor_msgs::CameraInfoPtr cinfo_msg(new sensor_msgs::CameraInfo(cinfo->getCameraInfo()));

    VideoCapture cap("/home/kbipin/catkin_bebop/bag/bebop-3.avi");

    cv::Mat frame;

    double time =(double)ros::Time::now().toSec();
    while((double)ros::Time::now().toSec() < time+20);

    while (ros::ok())
    {
        bool bSuccess = cap.read(frame); // read a new frame from camera feed

        if(!bSuccess) //test if frame successfully read
        {
            ROS_INFO("ERROR READING FRAME FROM CAMERA FEED");
            break;
        }


        image_msg_ptr = cv_bridge::CvImage(std_msgs::Header(), "bgr8", frame).toImageMsg();

        cinfo_msg->header.stamp = image_msg_ptr->header.stamp;
        cinfo_msg->header.frame_id = image_msg_ptr->header.frame_id;
        cinfo_msg->width = image_msg_ptr->width;
        cinfo_msg->height = image_msg_ptr->height;
        camera_pub.publish(image_msg_ptr, cinfo_msg);

        ros::spinOnce();
        loop_rate.sleep();
    }

}
