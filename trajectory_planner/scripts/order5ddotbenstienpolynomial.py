__author__ = 'kripalu'
from cvxopt import matrix
#B = matrix(0, (1, 6))

def order5ddotbenstienpolynomial(x):
	B = [];
	B.append(20*(1-x)**3)
	B.append(5 * (-8*(1-x)**3 + 12 * x * (1-x)**2))
	B.append(10 * (-12 * x * (1-x)**2 + 2 * (1-x)**3 + 6 * x**2 * (1-x) ))
	B.append(10 * (6 * x * (1-x)**2 - 12 * x**2 * (1-x) + 2*x**3))
	B.append(5 * (12 * x**2 * (1-x) - 8 * x**3))
	B.append(20 * x**3)
	return B
    #B[0] = 20*(1-x)**3
    #B[1] = 5 * (-8*(1-x)**3 + 12 * x * (1-x)**2)
    #B[2] = 10 * (-12 * x * (1-x)**2 + 2 * (1-x)**3 + 6 * x**2 * (1-x) )
    #B[3] = 10 * (6 * x * (1-x)**2 - 12 * x**2 * (1-x) + 2*x**3)
    #B[4] = 5 * (12 * x**2 * (1-x) - 8 * x**3)
    #B[5] = 20 * x**3
    