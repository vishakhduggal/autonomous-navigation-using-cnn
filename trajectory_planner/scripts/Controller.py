from BasisFunctions import Bspline_basis
from Constraints_Next import Constraints_Next
from BasisFunctions import BSplineDrev
from BasisFunctions import WhichSpan
from BsplineControlPoints import BsplineControlPoints
from cvxopt import matrix
from Constraints import Constraints
from Bezier import Bezier
from numpy import dot
from math import atan
from math import cos
# import Bspline
import time

order = 6
k = [0, 0, 0, 0, 0, 0, 0.1428, 0.2856, 0.4284, 0.5712, 0.7140, 0.8568, 1, 1, 1, 1, 1, 1]
n = 6
t = [0, 0, 0, 0, 0, 0, 0.1428, 0.2856, 0.4284, 0.5712, 0.7140, 0.8568, 1, 1, 1, 1, 1, 1]
u = [0, 0.1428, 0.2856, 0.4284, 0.5712, 0.7140, 0.8568, 1]
new_u = [0, 0, 0, 0, 0, 0, 0, 0];


b = []
dot_bsplien = []
ddot_bsplien = []
dddot_bsplien = []

order5bensteinpolynomialvar0 = [1, 0, 0, 0, 0, 0]
order5bensteinpolynomialvar1 = [0.462818608057538, 0.385502200365238, 0.128440770443668,0.0213968059021882, 0.00178223511597788, 5.93801153900237e-05]
order5bensteinpolynomialvar2 = [0.186083227234367,0.371958074595012, 0.297399849116280, 0.118893332737416, 0.0237653526244442,0.00190016369248076]
order5bensteinpolynomialvar3 = [0.0610184400473922, 0.228659024810207, 0.342748517245250,0.256881498929086, 0.0962631509282892, 0.0144293680397758]
order5bensteinpolynomialvar4 = [0.0144968579501837,0.0965555650786492, 0.257241318903566, 0.342668473315571, 0.228232546592647, 0.0608052381593843]
order5bensteinpolynomialvar5 = [0.00191350748617600, 0.0238853906491200, 0.119259922541760,0.297732813618240, 0.371645505110880, 0.185562860593824]
order5bensteinpolynomialvar6=  [6.02164418638643e-05,0.00180144718536868, 0.0215569825198866, 0.128980604909489, 0.385860971670567,0.461739777272824]
order5bensteinpolynomialvar7 = [1.02399999999944e-17, 1.27948799999944e-13, 6.39488102399789e-10,1.59808076789725e-06, 0.00199680191948783, 0.998001599360128]


dot_b = matrix(
    [-35.0140056022409, 35.0140056022409, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -2.18837535014006, -3.80939412802158,
     3.20151208631601, 2.50447401182695, 0.291783380018674, 0, 0, 0, 0, 0, 0, 0, 0, -0.648407511152609,
     -3.24203755576305, 0.680827886710239, 2.91783380018674, 0.291783380018674, 0, 0, 0, 0, 0, 0, 0, 0,
     -0.364729225023343, -2.84488795518207, 5.55111512312578e-16, 2.91799717273994, 0.291620007465472, 0, 0, 0, 0, 0, 0,
     0, 0, -0.291783380018674, -2.91783380018674, 0.00159302530182870, 2.84380521904481, 0.364218935858781, 0, 0, 0, 0,
     0, 0, 0, 0, -0.291783380018675, -2.91821373227956, -0.675910781803136, 3.23931326403499, 0.646594630066385, 0, 0,
     0, 0, 0, 0, 0, 0, -0.293186914122505, -2.50754221849167, -3.18637256519332, 3.81094334700760, 2.17615835079989, 0,
     0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -34.9162011173184, 34.9162011173184], (12, 8)).T
ddot_b = matrix([980.784470651005, -1471.17670597651, 490.392235325503, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 61.2990294156878,
                 -38.5956851876553, -60.1638622042862, 29.2873140541620, 8.17320392209171, 0, 0, 0, 0, 0, 0, 0, 0,
                 18.1626753824260, 9.08133769121302, -51.7636248399142, 16.3464078441834, 8.17320392209172, 0, 0, 0, 0,
                 0, 0, 0, 0, 10.2165049026146, 14.3031068636605, -49.0392235325503, 16.3509841061779, 8.16862766009726,
                 0, 0, 0, 0, 0, 0, 0, 0, 8.17320392209171, 16.3464078441835, -49.0312110710317, 14.3093882162753,
                 10.2022110884813, 0, 0, 0, 0, 0, 0, 0, 0, 8.17320392209172, 16.3174284164378, -51.7219042497656,
                 9.11937751161742, 18.1118943996186, 0, 0, 0, 0, 0, 0, 0, 0, 8.18957860677388, 29.1866893665323,
                 -60.1870426869310, -38.1460418353915, 60.9568165490164, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 488.338477165293,
                 -1463.65135753732, 975.312880372023], (12, 8)).T
dddot_b = matrix([-20604.7157699791, 36058.2525974634, -18887.6561224808, 3434.11929499652, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                  -1287.79473562369, 2337.10896465041, -977.770077047620, -243.250116728920, 171.705964749826, 0, 0, 0,
                  0, 0, 0, 0, 0, -381.568810555169, 667.745418471545, -114.470643166550, -343.411929499652,
                  171.705964749826, 0, 0, 0, 0, 0, 0, 0, 0, -214.632455937282, 386.338420687108, -1.06581410364015e-13,
                  -343.315789541785, 171.609824791959, 0, 0, 0, 0, 0, 0, 0, 0, -171.705964749826, 343.411929499652,
                  -0.216230807965729, -385.821899666256, 214.332165724396, 0, 0, 0, 0, 0, 0, 0, 0, -171.705964749826,
                  343.475761308522, 113.473321985189, -665.745101729151, 380.501983185265, 0, 0, 0, 0, 0, 0, 0, 0,
                  -171.569384220123, 244.476955218291, 969.882405574304, -2323.39536625769, 1280.60538968522, 0, 0, 0,
                  0, 0, 0, 0, 0, 0, -3416.54718166016, 18769.5334495341, -35785.5186779024, 20432.5324100284],
                 (12, 8)).T


#X = [0,0.813344739674464,5.05955971690193,12.8650906264003,22.1097190323661,29.9229412763030,34.1803473848508,34.9999999776134]
#Y = [0,0.813344739674464,5.05955971690193,12.8650906264003,22.1097190323661,29.9229412763030,34.1803473848508,34.9999999776134]
#Z = [0,0.813344739674464,5.05955971690193,12.8650906264003,22.1097190323661,29.9229412763030,34.1803473848508,34.9999999776134]

#X = [0, 0.232384211335561, 1.44558849054341, 3.67574017897151, 6.31706258067602, 8.54941179322944, 9.76581353852880,
#     9.99999999360384]
#Y = [0, 0.232384211335561, 1.44558849054341, 3.67574017897151, 6.31706258067602, 8.54941179322944, 9.76581353852880,
#     9.99999999360384]
#Z = [0, 0.232384211335561, 1.44558849054341, 3.67574017897151, 6.31706258067602, 8.54941179322944, 9.76581353852880,
#     9.99999999360384]

#vmax = 0.95
#amax = 1
#jmax = 1

vmax = 2.0
amax = 4.0
jmax = 8.0

px = []
py = []
pz = []

X = [0]*8
Y = [0]*8
Z = [0]*8

def Controller(xo, xf, vox, vfx, aox, afx, yo, yf, voy, vfy, aoy, afy, zo, zf, voz, vfz, aoz, afz):
    inp = [xo, xf, vox, vfx, aox, afx, yo, yf, voy, vfy, aoy, afy, zo, zf, voz, vfz, aoz, afz]
    print inp
    px, py, pz = Bezier(inp)

    X[0] = dot(order5bensteinpolynomialvar0,px)
    X[1] = dot(order5bensteinpolynomialvar1,px)
    X[2] = dot(order5bensteinpolynomialvar2,px)
    X[3] = dot(order5bensteinpolynomialvar3,px)
    X[4] = dot(order5bensteinpolynomialvar4,px)
    X[5] = dot(order5bensteinpolynomialvar5,px)
    X[6] = dot(order5bensteinpolynomialvar6,px)
    X[7] = dot(order5bensteinpolynomialvar7,px)

    Y[0] = dot(order5bensteinpolynomialvar0,py)
    Y[1] = dot(order5bensteinpolynomialvar1,py)
    Y[2] = dot(order5bensteinpolynomialvar2,py)
    Y[3] = dot(order5bensteinpolynomialvar3,py)
    Y[4] = dot(order5bensteinpolynomialvar4,py)
    Y[5] = dot(order5bensteinpolynomialvar5,py)
    Y[6] = dot(order5bensteinpolynomialvar6,py)
    Y[7] = dot(order5bensteinpolynomialvar7,py)

    Z[0] = dot(order5bensteinpolynomialvar0,pz)
    Z[1] = dot(order5bensteinpolynomialvar1,pz)
    Z[2] = dot(order5bensteinpolynomialvar2,pz)
    Z[3] = dot(order5bensteinpolynomialvar3,pz)
    Z[4] = dot(order5bensteinpolynomialvar4,pz)
    Z[5] = dot(order5bensteinpolynomialvar5,pz)
    Z[6] = dot(order5bensteinpolynomialvar6,pz)
    Z[7] = dot(order5bensteinpolynomialvar7,pz)

    Bsplien_px = BsplineControlPoints(n, t, u, X, vox, vfx, aox, afx)
    Bsplien_py = BsplineControlPoints(n, t, u, Y, voy, vfy, aoy, afy)
    Bsplien_pz = BsplineControlPoints(n, t, u, Z, voz, vfz, aoz, afz)
    endconstraints = [vox, voy, voz, vfx, vfy, vfz, aox, aoy, aoz, afx, afy, afz]
    maxconstraints = [vmax, amax, jmax]
    ALPHA = Constraints(u, Bsplien_px, Bsplien_py, Bsplien_pz, dot_b, ddot_b, dddot_b, maxconstraints, endconstraints)
    #print(ALPHA)
    # Got the ALpha value
    #	p = [l[0] for l in ALPHA.tolist()]
    #	print(p);
    ALPHA = [x if x != 0 else 0.5 for x in ALPHA]
    Prev = 0
    #print(type(ALPHA))
    for i in range(0, 7):
        #	type(ALPHA[i])
        new_u[i + 1] = Prev + (u[i + 1] - u[i]) / pow(ALPHA[i], 0.5)
        Prev = new_u[i + 1]

    #new_t = [ zeros(1,n) u1(2:7) ones(1,n)*u1(8) ];
    #ones = [1]*n;

    #new_t = matrix([[0]*n,new_u[1:6],[var * 5 for var in [1]*n]])
    new_t = [0] * n + new_u[1:7] + [var * new_u[7] for var in [1] * n]
    #print(len(new_t))
    #print(new_t)
    b = []
    dot_bsplien = []
    ddot_bsplien = []
    dddot_bsplien = []

    
    for i in range(0, 8):
        #for k in range(0,12):
        #A = bspline_basis(k,n,new_t,new_u(i))
        span = WhichSpan(new_u[i], new_t, 17, 5)
        #b.append([]);
        b.append(Bspline_basis(span, new_u[i], 5, new_t))

        dot_bsplien.append(BSplineDrev(n, new_t, new_u[i], 1))
        ddot_bsplien.append(BSplineDrev(n, new_t, new_u[i], 2))
        dddot_bsplien.append(BSplineDrev(n, new_t, new_u[i], 3))
    endconstraintsX = [vox, vfx, aox, afx]
    endconstraintsY = [voy, vfy, aoy, afy]
    endconstraintsZ = [voz, vfz, aoz, afz]
    px1 = Constraints_Next(X, new_u, b, dot_bsplien, ddot_bsplien, dddot_bsplien, maxconstraints, endconstraintsX);
    py1 = Constraints_Next(Y, new_u, b, dot_bsplien, ddot_bsplien, dddot_bsplien, maxconstraints, endconstraintsY);
    pz1 = Constraints_Next(Z, new_u, b, dot_bsplien, ddot_bsplien, dddot_bsplien, maxconstraints, endconstraintsZ);

    #print px1
    #print py1
    #print pz1

    q= [0.01, 0.02, 0.03, 0.04, 0.05, 0.06, 0.07, 0.08, 0.09, .1]
    sumx = 0.0
    sumy = 0.0
    sumz = 0.0
    for i in q:
       b_drev_2 = BSplineDrev(n, new_t, new_u[0]+i, 2)
       sumx = sumx + dot(b_drev_2,px1)
       sumy = sumy + dot(b_drev_2,py1)
       sumz = sumz + dot(b_drev_2,pz1)

    ax=(sumx/10.0);
    ay=(sumy/10.0);
    az=(sumz/10.0);
    # Roll and Pitch calculations
    pitch = atan(ax/10.0)/(9.8 + az/10)
    roll =  (atan(-ay/10.0)/(9.8 + az/10))*cos(pitch.getA1()[0]);     

    return(roll.getA1()[0], pitch.getA1()[0], az)
    #return(roll, pitch, 0)
