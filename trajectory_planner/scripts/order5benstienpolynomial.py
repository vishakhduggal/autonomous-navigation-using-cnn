__author__ = 'kripalu'

from cvxopt import matrix
#B = matrix(0, (1, 6))

def order5benstienpolynomial(x):
    """ This function"""
    B = [];
    B.append((1-x)**5)
    B.append(5 * (1-x)**4 * x)
    B.append(10 * (1-x)**3 * x**2)
    B.append(10 * (1-x)**2 * x**3)
    B.append(5 * (1-x) * x**4)
    B.append(x**5)
    return B
    #B[0] = (1-x)**5
    #B[1] = 5 * (1-x)**4 * x
    #B[2] = 10 * (1-x)**3 * x**2
    #B[3] = 10 * (1-x)**2 * x**3
    #B[4] = 5 * (1-x) * x**4
    #B[5] = x**5
    


