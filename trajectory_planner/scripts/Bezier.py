__author__ = 'kripalu'
import sys
sys.path.append("/usr/lib/python2.7/dist-packages/numpy")

#from numpy import linalg
from numpy.linalg import pinv
from numpy import dot
import scipy
#import numpy as np
import time

from cvxopt import matrix
from order5dotbenstienpolynomial import order5dotbenstienpolynomial
from order5ddotbenstienpolynomial import order5ddotbenstienpolynomial
from order5benstienpolynomial import order5benstienpolynomial



def Bezier(inp):

    xo = inp[0]
    xf = inp[1]
    xdoto = inp[2]
    xdotf = inp[3]
    xddoto = inp[4]
    xddotf = inp[5]
    
    
    yo = inp[6]
    yf = inp[7]
    ydoto = inp[8]
    ydotf = inp[9]
    yddoto = inp[10]
    yddotf = inp[11]
    
    zo = inp[12]
    zf = inp[13]
    zdoto = inp[14]
    zdotf = inp[15]
    zddoto = inp[16]
    zddotf = inp[17]
    
    to = 0
    tf = 1

    B_to = order5benstienpolynomial(to)
    B_tf = order5benstienpolynomial(tf)
    B_dot_to = order5dotbenstienpolynomial(to)
    B_dot_tf = order5dotbenstienpolynomial(tf)
    B_ddot_to = order5ddotbenstienpolynomial(to)
    B_ddot_tf = order5ddotbenstienpolynomial(tf)
    
    #print(B_to)  
    #print(B_tf)
    A = matrix([B_to, B_dot_to, B_ddot_to, B_tf, B_dot_tf, B_ddot_tf]).T  
    #print(A)
    #A = [B_toB_dot_toB_ddot_toB_tfB_dot_tfB_ddot_tf]
    
    
   #  Solving for Ap = C  or p = inv(A).C
    Cx = matrix([xo, xdoto, xddoto, xf, xdotf, xddotf])
    #print(Cx)
    Cy = matrix([yo, ydoto, yddoto, yf, ydotf, yddotf])
    #print(Cy)
    Cz = matrix([zo, zdoto, zddoto, zf, zdotf, zddotf])
    #print(Cz)
    px = dot(pinv(A) ,Cx)
    py = dot(pinv(A) ,Cy)
    pz = dot(pinv(A) ,Cz)
    px = [l[0] for l in px.tolist()]
    py = [l[0] for l in py.tolist()]
    pz = [l[0] for l in pz.tolist()]

    return (px,py,pz)
