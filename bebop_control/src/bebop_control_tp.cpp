/*******************************************************************************
   Kumar Bipin
   kumar.bipin@research.iiit.ac.in

Uurmi System Lab, Hyderabad

This code is developed to test the control command to Ardrone 2.0
consequently which are use for System Identification Purpose
*********************************************************************************/

#include "bebop_control.h"
#include "bebop_perception/NavdataWaypoint.h"
#include "bebop_autonomy/NavdataVelocity.h"
#include "bebop_stateestimation.h"
#include <trajectory_planner/TrajectoryPlanner.h>

using namespace std;
using namespace cv;

#define  MAXLOOP 1000

#define HOVER     0
#define GOFORWARD 1
#define MOVELEFT  2
#define MOVERIGHT 3
#define EXPLORE   4

#define  HOVER    0
#define  PITCHMAX 10
#define  PITCHMIN 10
#define  ROLLMAX  3
#define  ROLLMIN  3
#define  YAWMAX   60
#define  YAWMIN   10
#define  GAZMAX   25
#define  GAZMIN   5

ros::Publisher takeoff_pub;
ros::Publisher landing_pub;
ros::Publisher cmd_pub;
ros::ServiceClient trajectoryPlannerClient;

trajectory_planner::TrajectoryPlanner trajectoryPlannerSrv;

bool firstRunExp=false;

static struct joystick joy;

static unsigned int simplecmd;
static unsigned int counter=0;
static float deltaheading; 

typedef union {
  float f;
  struct {
    unsigned int mantisa : 23;
    unsigned int exponent : 8;
    unsigned int sign : 1;
  } parts;
} double_cast;

struct Bebop_State state;

struct Attitude Atd;
struct Velocity Vt;
struct Acceleration At;
struct Position Xt;

void velocity_callback(const bebop_autonomy::NavdataVelocity &msg_in)
{
    Vt.vx=msg_in.Vx;
    Vt.vy=msg_in.Vy;
    Vt.vz=msg_in.Vz;
}
void attitude_callback(const bebop_autonomy::NavdataAttitude &msg_in)
{
    state.roll=msg_in.roll;
    state.pitch=msg_in.pitch;
    state.yaw=msg_in.yaw;
}

void joy_callback(const sensor_msgs::Joy& joy_msg_in)
{
    //Take in PS3 xbox controller

    joy.x=joy_msg_in.axes[5];                     //left stick up-down
    joy.y=joy_msg_in.axes[4];                     //left stick left-right

    joy.z=joy_msg_in.buttons[6];                  //Left Bottom  button
    if(!joy.z) joy.z=-joy_msg_in.buttons[7];      //right Bottom  button

    joy.yaw=joy_msg_in.buttons[4];                //Left Upper
    if(!joy.yaw) joy.yaw=-joy_msg_in.buttons[5];  //Right Lower

    joy.takeoff=joy_msg_in.buttons[3];            //Y button
    joy.land=joy_msg_in.buttons[0];                      //X button
    joy.emergency=joy_msg_in.buttons[2];              // button

    joy.exp=joy_msg_in.buttons[1];                //Cross button
}

struct command bebop_map_progessive_cmd(float pitch , float roll, float gaz, float yaw)
{
    struct command cmd;

    if((counter <= MAXLOOP) || (pitch < PITCHMAX))
    {
        cmd.pitch = PITCHMIN;
    }
    else
    {
        cmd.pitch = PITCHMAX;
    }
    if((counter <= MAXLOOP) || (roll < ROLLMAX))
    {
        cmd.roll = ROLLMIN;
    }
    else
    {
        cmd.roll =ROLLMAX;
    }
    if((counter <= MAXLOOP) || (gaz < GAZMAX))
    {
        cmd.gaz = GAZMIN;
    }
    else
    {
        cmd.gaz = GAZMAX;
    }
    if((counter <= MAXLOOP) || (yaw < YAWMAX))
    {
        cmd.yaw = YAWMIN;
    }
    else
    {
        cmd.yaw = YAWMAX;
    }
    if ( counter > MAXLOOP)
    {
        cmd.pitch=cmd.roll=cmd.gaz=cmd.yaw=HOVER;
    }
    return(cmd);
}

void set_waypoint_forward()
{

    trajectoryPlannerSrv.request.xo = state.x;
    trajectoryPlannerSrv.request.xdoto =state.vx+0.01;
    trajectoryPlannerSrv.request.xddoto = state.ax;

    trajectoryPlannerSrv.request.yo = state.y;
    trajectoryPlannerSrv.request.ydoto = state.vy+0.01;
    trajectoryPlannerSrv.request.yddoto = state.ay;

    trajectoryPlannerSrv.request.zo = state.z;
    trajectoryPlannerSrv.request.zdoto = state.vz + 0.01;
    trajectoryPlannerSrv.request.zddoto = state.az;


    trajectoryPlannerSrv.request.xf = 10;
    trajectoryPlannerSrv.request.xdotf = 0.0;
    trajectoryPlannerSrv.request.xddotf = 0.0;

    trajectoryPlannerSrv.request.yf = 0;
    trajectoryPlannerSrv.request.ydotf = 0.0;
    trajectoryPlannerSrv.request.yddotf = 0.0;

    trajectoryPlannerSrv.request.zf = 0.80;
    trajectoryPlannerSrv.request.zdotf = 0.0;
    trajectoryPlannerSrv.request.zddotf = 0.0;
}

void set_waypoint_left()
{

    trajectoryPlannerSrv.request.xo = state.x;
    trajectoryPlannerSrv.request.xdoto =state.vx+0.01;
    trajectoryPlannerSrv.request.xddoto = state.ax;

    trajectoryPlannerSrv.request.yo = state.y;
    trajectoryPlannerSrv.request.ydoto = state.vy+0.01;
    trajectoryPlannerSrv.request.yddoto = state.ay;

    trajectoryPlannerSrv.request.zo = state.z;
    trajectoryPlannerSrv.request.zdoto = state.vz + 0.01;
    trajectoryPlannerSrv.request.zddoto = state.az;


    trajectoryPlannerSrv.request.xf = 10;
    trajectoryPlannerSrv.request.xdotf = 0.0;
    trajectoryPlannerSrv.request.xddotf = 0.0;

    trajectoryPlannerSrv.request.yf = -3;
    trajectoryPlannerSrv.request.ydotf = 0.0;
    trajectoryPlannerSrv.request.yddotf = 0.0;

    trajectoryPlannerSrv.request.zf = 0.80;
    trajectoryPlannerSrv.request.zdotf = 0.0;
    trajectoryPlannerSrv.request.zddotf = 0.0;
}
void set_waypoint_right()
{

    trajectoryPlannerSrv.request.xo = state.x;
    trajectoryPlannerSrv.request.xdoto =state.vx+0.01;
    trajectoryPlannerSrv.request.xddoto = state.ax;

    trajectoryPlannerSrv.request.yo = state.y;
    trajectoryPlannerSrv.request.ydoto = state.vy+0.01;
    trajectoryPlannerSrv.request.yddoto = state.ay;

    trajectoryPlannerSrv.request.zo = state.z;
    trajectoryPlannerSrv.request.zdoto = state.vz + 0.01;
    trajectoryPlannerSrv.request.zddoto = state.az;


    trajectoryPlannerSrv.request.xf = 10;
    trajectoryPlannerSrv.request.xdotf = 0.0;
    trajectoryPlannerSrv.request.xddotf = 0.0;

    trajectoryPlannerSrv.request.yf = 3;
    trajectoryPlannerSrv.request.ydotf = 0.0;
    trajectoryPlannerSrv.request.yddotf = 0.0;

    trajectoryPlannerSrv.request.zf = 0.80;
    trajectoryPlannerSrv.request.zdotf = 0.0;
    trajectoryPlannerSrv.request.zddotf = 0.0;
}
void flightPlanningCallback(const std_msgs::String::ConstPtr& msg)
{

   if(strcmp(msg->data.c_str(), "GoStraight") == 0)
     {
         simplecmd=GOFORWARD;
     }
   else if(strcmp(msg->data.c_str(), "MoveLeft") == 0)
     {
         simplecmd=MOVELEFT;
     }
   else if(strcmp(msg->data.c_str(), "MoveRight") == 0)
     {
        simplecmd=MOVERIGHT;
     }
   else if(strcmp(msg->data.c_str(), "Explore") == 0)
     {
        simplecmd=EXPLORE;
     }
   else if(strcmp(msg->data.c_str(), "Hover") == 0)
     {
       simplecmd=HOVER;
     }
     //ROS_INFO("I heard: [%s], %d", msg->data.c_str(), simplecmd);
}

void wait_for_time(unsigned int wait_time)
{
    unsigned long i=0, millisecond;
    clock_t start_t,end_t;
    start_t = clock();
    while(1)
    {
        end_t = clock();
        millisecond = (end_t - start_t) * 1000 / CLOCKS_PER_SEC;
        if (millisecond > wait_time)
        {
            break;
        }
        for(i=0;i<0xFF;i++);
    }
}

void send_control_cmd(int flag, int roll , int pitch, int yaw, int gaz)
{

    bebop_autonomy::dataPCMD cmd;
    
    cmd.flag = flag;
    cmd.roll = roll;
    cmd.pitch = pitch;
    cmd.yaw =  yaw;
    cmd.gaz =  gaz;

    cmd_pub.publish(cmd);
    if( !((pitch==0.0) && (roll==0.0) && (gaz == 0.0) && (yaw == 0.0)))
        wait_for_time(200);

}

void autonomous_flight_cruise()
{
     struct command control;
     double_cast d;
     d.f=deltaheading;
     
     if(simplecmd==GOFORWARD)
      {
        ROS_INFO("I heard:%d", simplecmd);
       if(fabs(fabs(deltaheading - 20.0) >= 4.0))
        {
          if(d.parts.sign == 1)
            {
              set_waypoint_right();
              trajectoryPlannerClient.call(trajectoryPlannerSrv);
              control=bebop_map_progessive_cmd(trajectoryPlannerSrv.response.pitch, trajectoryPlannerSrv.response.roll, trajectoryPlannerSrv.response.gaz, trajectoryPlannerSrv.response.yaw);
              send_control_cmd(1, control.roll, control.pitch, 0, 0);
            }
          if(d.parts.sign == 0)
            {
              set_waypoint_left();
              trajectoryPlannerClient.call(trajectoryPlannerSrv);
              control=bebop_map_progessive_cmd(trajectoryPlannerSrv.response.pitch, trajectoryPlannerSrv.response.roll, trajectoryPlannerSrv.response.gaz, trajectoryPlannerSrv.response.yaw);
              send_control_cmd(1, -control.roll, control.pitch, 0, 0);
            }      
        }
       else
        {

          ROS_INFO("I heard:KBP %d", simplecmd);
          set_waypoint_forward();
          trajectoryPlannerClient.call(trajectoryPlannerSrv);
          control=bebop_map_progessive_cmd(trajectoryPlannerSrv.response.pitch, trajectoryPlannerSrv.response.roll, trajectoryPlannerSrv.response.gaz, trajectoryPlannerSrv.response.yaw);
          send_control_cmd(1, 0, control.pitch, 0, 0);
        }
      }
      else if(simplecmd==MOVELEFT)
      {
        ROS_INFO("I heard:%d", simplecmd);
        send_control_cmd(1, -3, 0, 0, 0);
      }
      else if(simplecmd==MOVERIGHT)
      {
        ROS_INFO("I heard:%d", simplecmd);
        send_control_cmd(1, 3, 0, 0, 0);
      }
      else if(simplecmd==EXPLORE)
      {
        ROS_INFO("I heard:%d", simplecmd);
        send_control_cmd(1, 0, 0, 60, 0);
      }
      else if(simplecmd==HOVER)
      {
        ROS_INFO("I heard:%d", simplecmd);
        send_control_cmd(0, 0, 0, 0, 0);
      }

}

void deltaCallback(const bebop_perception::NavdataWaypoint::ConstPtr &msg_in)
{
    float deltaX = msg_in->deltaX;
    float deltaY = msg_in->deltaY;
    deltaheading = msg_in->deltaheading;
    //ROS_INFO("deltaX=%f, deltaY=%f delatHeading=%f", deltaX, deltaY, deltaheading);

}

void bebop_update_state(struct Position Xt, struct Velocity Vt, struct Acceleration At, struct Attitude Atd)
{
    state.x=Xt.x;
    state.y=Xt.y;
    state.z=Xt.z;

    state.vx =Vt.vx; 
    state.vy =Vt.vy; 
    state.vz =Vt.vz;
 
    state.ax = At.ax; 
    state.ay = At.ay;
    state.az = At.az;

    state.roll = Atd.roll;
    state.pitch =Atd.pitch;
    state.yaw = Atd.yaw;

    
}

int main(int argc, char** argv)
{

    double dt=0.2;
    unsigned int looprate=50;
    bool takeoff=false;

    ros::init(argc, argv,"Bebop_fly_test");
    ros::NodeHandle nh_;
    ros::Rate loop_rate(looprate);
    ros::Subscriber joy_sub;
    ros::Subscriber flightPlanning_sub;
    ros::Subscriber sub_delta;
    ros::Subscriber velocity_sub;
    ros::Subscriber attitude_sub;

    /*********************** Subscribe ************** */

    joy_sub = nh_.subscribe("/joy", 1, joy_callback);
    flightPlanning_sub = nh_.subscribe("/flightPlanning/cmd", 1, flightPlanningCallback);
    sub_delta = nh_.subscribe("/bebop_perception/navdata", 10, deltaCallback);
    velocity_sub = nh_.subscribe("/bebop/velocity", 1, velocity_callback);
    attitude_sub = nh_.subscribe("/bebop/attitude", 1, attitude_callback);

    /*********************** Advertise ************** */
    std::string takeoff_channel = nh_.resolveName("/bebop/takeoff");
    takeoff_pub  = nh_.advertise<std_msgs::Empty>(takeoff_channel,1);

    std::string landing_channel = nh_.resolveName("/bebop/landing");
    landing_pub  = nh_.advertise<std_msgs::Empty>(landing_channel,1);

    std::string control_channel = nh_.resolveName("/bebop/cmdvel");
    cmd_pub = nh_.advertise<bebop_autonomy::dataPCMD>(control_channel,1);


    trajectoryPlannerClient = nh_.serviceClient<trajectory_planner::TrajectoryPlanner>("trajectoryPlanner");

    double time =(double)ros::Time::now().toSec();
    while((double)ros::Time::now().toSec() < time+3);

    while (ros::ok())
    {

        Xt=bebop_estimate_position(Vt.vx, Vt.vy, Vt.vz, dt);
        At=bebop_estimate_acceleration(Vt.vx, Vt.vy, Vt.vz, dt);
        bebop_update_state(Xt, Vt, At, Atd);
        /*commands to change state of drone*/
        if(joy.takeoff)
        {
            ROS_INFO("#...TAKEOFF...#");
            takeoff_pub.publish(std_msgs::Empty());
            firstRunExp=false;

            memset(&joy, 0, sizeof(struct joystick));
        }
        if(joy.land)
        {
            ROS_INFO("#...LANDING...#");
            landing_pub.publish(std_msgs::Empty());
            firstRunExp=false;

            memset(&joy, 0, sizeof(struct joystick));
        }

        if(joy.emergency)
        {
            ROS_INFO("#...LANDING...#");
            landing_pub.publish(std_msgs::Empty());
            //emergency_pub.publish(std_msgs::Empty());
            firstRunExp=false;

            memset(&joy, 0, sizeof(struct joystick));
        }
        if(joy.exp)
        {
            if(!firstRunExp)
            {
                ROS_INFO("#...EXPERIMENT STARTED...#");
                firstRunExp=true;
            }
            memset(&joy, 0, sizeof(struct joystick));
        }
        if(abs(joy.x))
        {
            if(joy.x > 0)
            {
                ROS_INFO("#...FORWARD...#");
                send_control_cmd(1, 0, 12, 0, 0);
            }
            else
            {
                ROS_INFO("#...BACKWARD...#");
                send_control_cmd(1, 0, -25, 0, 0);
            }
            firstRunExp=false;
            memset(&joy, 0, sizeof(struct joystick));
        }
        else if(abs(joy.y))
        {
            if(joy.y > 0)
            {
                ROS_INFO("#...LEFT...#");
                send_control_cmd(1, -10, 0, 0, 0);
            }
            else
            {
                ROS_INFO("#...RIGHT...#");
                send_control_cmd(1, 10, 0, 0, 0);
            }
            firstRunExp=false;
            memset(&joy, 0, sizeof(struct joystick));
        }
        else if(abs(joy.z))
        {
            if(joy.z > 0)
            {
                ROS_INFO("#...DOWN...#");
                send_control_cmd(0, 0, 0, 0, -50);
            }
            else
            {
                ROS_INFO("#...UP...#");
                send_control_cmd(0, 0, 0, 0, 50);
            }
            firstRunExp=false;
            memset(&joy, 0, sizeof(struct joystick));
        }
        else if(abs(joy.yaw))
        {

            if(joy.yaw > 0)
            {
                ROS_INFO("#...ANTI-CLOCKWISE...#");
                send_control_cmd(0, 0, 0, -20, 0);
            }
            else
            {
                ROS_INFO("#...CLOCKWISE...#");
                send_control_cmd(0, 0, 0, 20, 0);
            }
            firstRunExp=false;
            memset(&joy, 0, sizeof(struct joystick));
        }
        else if(firstRunExp)
        {
            /***************************************************************************/
            if(counter <= MAXLOOP)
            {
                 ROS_INFO("#... Autonomous Flight ... .. .");
                 autonomous_flight_cruise();
                 
            }
            else
            {
                firstRunExp = false;
                ROS_INFO("#... Manual Flight ... .. .");
                goto done;
            }
            counter++;
            /**************************************************************************/
        }
        else
        {
            //ROS_INFO("#...HOVER...#");
            send_control_cmd(0, 0, 0, 0, 0);
            
            firstRunExp=false;
            memset(&joy, 0, sizeof(struct joystick));
        }
done:
        ros::spinOnce();
        loop_rate.sleep();
    }/*ros::ok*/

    ROS_ERROR("ROS::OK failed- Node Closing");
    ros::shutdown();

}//main
